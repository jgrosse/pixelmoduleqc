
#first clone labRemote into the project directory
git clone https://:@gitlab.cern.ch:8443/berkeleylab/labRemote.git --branch devel

mkdir results
mkdir build; cd build
cmake3 ..
make -j

cd ..
