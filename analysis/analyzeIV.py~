# Code used to analyze IV curves
# inputs:
#    ifile: input file name (i.e. IVout_test)
#    ofile: output file name
# outputs: saves a plot of IV curve with fit function as .png (default), (optional: .eps or .pdf)
# author: E. Resseguie
from pylab import *
import numpy
import matplotlib.pyplot as plt
import os, glob, getopt
import json

matplotlib.rcParams.update({'font.size': 12}) #sets the font on the plots

colors='blue'

# function to plot the IV curve and fit a linear function
# saves the output as .png, .eps, .pdf
# description of the arguments:
# x: x variable to plot, y: y variable to plot
# xtitle: x-axis title, ytitle: y-axis title
# title: plot title
# save: output 
def superpose(x,y, xtitle, ytitle, title, save,yerr=None, savetype='png'):
    fig, ax = plt.subplots()

    # Plot the current and voltage
    if (yerr == None):
        ax.plot(x,y, ls='', marker='.')
    else:
        ax.errorbar(x, y, yerr = yerr, linestyle = "None", marker='.',markersize=5, color=colors)
        
    ax.set_title(title,fontsize=12)
    ax.set_ylabel(ytitle,fontsize=12)
    ax.set_xlabel(xtitle,fontsize=12)
    ax.grid(True)

    # save formats
    plt.savefig('../results/'+save+'.'+savetype)

    plt.clf()

def main(argv):
   inputfile = ''
   outputfile = ''
   minVolt = None
   maxVolt = None
   
   opts, args = getopt.getopt(argv,"hi:o:s:e:",["ifile=","ofile="])
   if len(opts) == 0:
      print('Missing input filename and output filname!')
      print('Try analyzeIV.py -i <inputfile> -o <outputfile>')
      print('For full list of options run analyzeIV.py -h')
   for opt, arg in opts:
      if opt == '-h':
         print('analyzeIV.py -i <inputfile> -o <outputfile> -s <savetype>')
         sys.exit()
      elif opt in ("-i", "--ifile"):
         inputfile = arg
      elif opt in ("-o", "--ofile"):
         outputfile = arg
      elif opt in ("-s", "--savetype"):
          savetype = arg

   if inputfile == '':
      print('Missing input file. Try analyzeIV.py -i <inputfile>')
      sys.exit()

   if outputfile == '': outputfile = "test"
   if minVolt == None: minVolt = 1.4
   if savetype == '': savetype = 'png'
   
   #getting the list of input files
   list_of_files = glob.glob('../results/'+inputfile)
      
   #define arrays   
   curr, volt=([] for i in range(0,2))

   chipName=''
   
   #start processing the files one by one
   for fileName in list_of_files:

       #open json file to read in the current, voltage, and chip status code
       with open(fileName) as json_file:
           data = json.load(json_file)
           chipName = data['chipName']
           values = data['values']
           curr.append(values['Current'])
           volt.append(values['Voltage'])

   #make plots
   superpose(volt, curr, 'Voltage [V]', 'Current [A]', chipName+' IV curve', chipName+'_'+outputfile,None, savetype)
        
if __name__ == "__main__":
   main(sys.argv[1:])
   




