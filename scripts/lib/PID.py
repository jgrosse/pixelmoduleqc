class PID():
    def __init__(self, Kp, Ki, Kd):
        self.sample_time=100.0
        self.set_tunings(Kp,Ki,Kd)
        self.first_compute=True
        self.output_sum=0
        self.last_error=0
    
    def compute(self, inp, set_point, duration):
        if(duration>=self.sample_time):
            error = inp - set_point
            delta_error = error - self.last_error
            self.output_sum += (self.ki * error)
            output = (self.kp * error) + self.output_sum + \
                (delta_error * self.kd)
            if(output > self.out_max): output = self.out_max
            elif(output < self.out_min): output = self.out_min
            self.last_error = error
            self.first_compute = False
            return [True,output]
        else:
            return [False,0]
    
    def set_output_limits(self, minimum, maximum):
        self.out_min=minimum
        self.out_max=maximum
    
    def set_tunings(self, Kp, Ki, Kd):
        sample_time_sec = self.sample_time / 1000.0
        self.kp = Kp
        self.ki = Ki * sample_time_sec;
        self.kd = Kd / sample_time_sec;
    
    def set_sample_time(self, new_time):
        self.sample_time = float(new_time)
    
    def get_kp(self):
        return self.kp
    
    def get_ki(self):
        return self.ki
    
    def get_kd(self):
        return self.kd
