# Controls temperature using a peltier. Configuration 
# is specified by peltier_cooling.json.

import json, time
from labRemote import ec
from lib import PeltierSensors, PID

# Returns the configuration data stored
# in peltier_cooling.json
def get_config():
    config_file='peltier_cooling.json'
    f=open(config_file,'r')
    return json.load(f)

# Returns the settings stored in config
def get_settings(config):
    return config['settings']

# Returns the PID settings stored in config
def get_pid_settings(config):
    settings=config['PIDsettings']
    set_point=273.15+settings['set_point']
    max_voltage=float(config["PS"]['max_voltage'])
    go_time_temp=max_voltage/float(settings['kp'])
    duration=float(settings['duration'])
    return [set_point, max_voltage, go_time_temp, duration]

# Creates PeltierSensors objects for the two NTC sensors
def make_sensors(config):
    arduino=config["arduino"]
    return PeltierSensors.PeltierSensors(arduino['port'], \
        arduino['hot_ntc_pin'],arduino['cold_ntc_pin'], \
        arduino['refTemp'],arduino['Rref'],arduino['Bntc'], \
        arduino['V'],arduino['Rdiv'],arduino['sht85_address'])

# Creates an interface for contolling the power supply
def make_ps(config):
    PS=config["PS"]
    hw=ec.EquipConf()
    hw.setHardwareConfig(PS["config_file"])
    return hw.getPowerSupplyChannel(PS["channel"])

# Initializes power supply and turns it on
def init_ps(ps,max_voltage):
    ps.program()
    if(ps.getVoltageLevel()>max_voltage):
        print("Voltage too high!")
        ps.turnOff()
        raise Exception
    ps.turnOn()

def make_pid(config):
    settings=config["PIDsettings"]
    pid=PID.PID(float(settings["kp"]),float(settings["ki"]),float(settings["kd"]))
    pid.set_output_limits(1.0, float(config["PS"]["max_voltage"]))
    return pid

def main():
    config_data=get_config()
    settings=get_settings(config_data)
    sleep_time=float(settings['sleep'])
    [set_point, max_voltage, go_time_temp, duration]=get_pid_settings(config_data)
    
    sensors=make_sensors(config_data)
    powersupply=make_ps(config_data)
    init_ps(powersupply,max_voltage)
    pid=make_pid(config_data)
    
    voltage=0
    temp_reached = False
    temp=0
    hot_temp=0

    try:
     while (True):
         temp=sensors.read_cold()+273.15
         hot_temp=sensors.read_hot()+273.15
         if(temp==0 or hot_temp==0):
             print("Problem reading NTCs. Continuing...")
             time.sleep(sleep_time)
             continue
         try:
             [temp_sht,dew_point]=sensors.read_sht85()
         except ValueError:
             print("Problem reading SHT85. Continuing...")
             time.sleep(sleep_time)
             continue
         temp_sht+=273.15
         dew_point+=273.15
         
         print("Cold temp: "+str(temp-273.15))
         print("Hot temp: "+str(hot_temp-273.15))
         print("Dew point: "+str(dew_point-273.15))
         print("SHT85 temp:"+str(temp_sht-273.15))
         
         if(hot_temp>30+273.15):
             print("Heat sink is too hot! Turning power supply off.")
             powersupply.turnOff()
             break
         elif(temp<dew_point-5.0):
             print("Approaching dew point! Turning power supply off.")
             powersupply.turnOff()
             break
         try:
             if(abs(set_point - temp) < go_time_temp or temp_reached):
                 temp_reached = True;
                 [success,voltage]=pid.compute(temp, set_point, duration)
                 if (success):
                     print("Power supply voltage: " + str(voltage))
                     powersupply.setVoltageLevel(voltage);
             elif(temp < set_point):
                 print("Power supply voltage: 0")
                 # Setting this below 1.0 may cause errors
                 powersupply.setVoltageLevel(1.0)
             else:
                 print("Power supply voltage: " + str(max_voltage))
                 powersupply.setVoltageLevel(max_voltage)
         except Exception as e:
             print(e)
             continue
         time.sleep(sleep_time)
    except Exception as e:
        print(e)
        print("Turning off power supply.")
        powersupply.turnOff()

if __name__=="__main__" : main()
